# MSU Header Shim

## Why is this needed?

This project is creates a Javascript shim that will automatically modify a banner element to display MSU branding, as requested by the central communications office of the University.  According to the [MSU Brand Standards document, version 4](http://cabs.msu.edu/documents/msu_brand_standards_v4.pdf) on page 45 the following two points are made:

 * "To provide immediate brand recognition and connect university units with the Michigan State University brand, print and online publications and websites for all MSU units should reflect consistent graphic identity standards."
 * "IncludetheMichiganStateUniversitywordmarkoneveryuniversitywebsite, in the footer if using the primary MSU banner design (recommended) or in the upper left corner of the banner if not using a design consistent with the university home page."

## How do I use it?

This shim can be added to your webpage if you :
 
 0) put the msu-header-shim.js javascript file in your website folder
 
 1) place the following in your web page, just after the body tag, with your unit name between the div tags (no other elements such as p or h1 are necessary)
 
 <div id="banner">Your Unit Name</div>
 
 2) placing the following script tag after your body tag, therefore it won't run until after the full DOM has loaded.
 
 <script type="text/javascript" src="assets/javascripts/msu-header-shim.js"></script>

See the index.html as an example page. 

## Accessibility

If Javascript is turned off, or the page is ready by a screen reader or search engine, the unit name will appear but will not be styled (unless you add CSS for the #banner ID). 

## Compatibility

Compatible with all current web browsers, and many old versions (IE 5.5 + , Safari 4+, Firefox 4+). 

## Contributors

 * Bills, Patrick (billspat@msu.edu)
 * Murray, Troy (tm@msu.edu)

## How Do I Contribute?

You can contribute to this project by doing the following:

1. Email Patrick or Troy and request write access to the repository
2. If you haven't already, checkout your own version of the repository
  ```git clone ```
3. 
