// constants
var img_url = 'http://tech.msu.edu/_assets/img/logo-michiganstateuniversity.png';
var title_style = 'font-size: 19px; color: white; padding-top: 5px; margin-left: 1.7%; float: left; font-weight: 100; font-family: Helvetica, Arial, sans-serif; text-transform: capitalize;';

// get div from source html instead, and title from that
var msu = document.getElementById('banner')
var title = banner.innerHTML; // no default given 

msu.innerHTML = '<img src="' + img_url + '" style="margin-left: 15.0%; float: left; margin-top: 10px;">';
msu.innerHTML = banner.innerHTML + '<h1 style="' + title_style + '">' + title + '</h1>';
msu.setAttribute("style","width: 110%; height: 60px; margin-left: -10px; margin-top: -10px; clear: both; background-image: linear-gradient(bottom, #1A493E 73%, #153E35 86%); background-image: -o-linear-gradient(bottom, #1A493E 73%, #153E35 86%); background-image: -moz-linear-gradient(bottom, #1A493E 73%, #153E35 86%); background-image: -webkit-linear-gradient(bottom, #1A493E 73%, #153E35 86%); background-image: -ms-linear-gradient(bottom, #1A493E 73%, #153E35 86%); background-image: -webkit-gradient(linear, left bottom, left top, color-stop(0.73, #1A493E), color-stop(0.86, #153E35) );");


// document.body.insertBefore(msu, document.body.firstChild);
